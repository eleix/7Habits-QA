-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: 7_habits
-- ------------------------------------------------------
-- Server version	5.5.47-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'user',
  `email` varchar(255) NOT NULL,
  `regip` varchar(255) NOT NULL,
  `experience` int(255) DEFAULT '0',
  `level` int(11) DEFAULT '0',
  `rank` int(11) DEFAULT '0',
  `correctansw` int(11) DEFAULT '0',
  `incorrectansw` int(11) DEFAULT '0',
  `lastip` varchar(255) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `banned` int(11) NOT NULL DEFAULT '0',
  `isonline` int(11) NOT NULL DEFAULT '0',
  `regdate` datetime NOT NULL,
  `lastlogindate` datetime DEFAULT NULL,
  `emailenabled` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `totalbanned_idx` (`banned`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `7_habits`.`accounts_BEFORE_INSERT` BEFORE INSERT ON `accounts` FOR EACH ROW
BEGIN
	SET NEW.regdate = NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `7_habits`.`accounts_BEFORE_UPDATE` BEFORE UPDATE ON `accounts` FOR EACH ROW
BEGIN
	SET NEW.lastlogindate= NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timesent` datetime DEFAULT NULL,
  `senderid` int(11) NOT NULL,
  `recieverid` int(11) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `body` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamelogs`
--

DROP TABLE IF EXISTS `gamelogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamelogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `alerttime` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamelogs`
--

LOCK TABLES `gamelogs` WRITE;
/*!40000 ALTER TABLE `gamelogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamelogs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `7_habits`.`gamelogs_BEFORE_INSERT` BEFORE INSERT ON `gamelogs` FOR EACH ROW
BEGIN
	SET NEW.alerttime = NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `Question` varchar(255) NOT NULL,
  `correctanswer` varchar(255) NOT NULL,
  `Level` int(11) NOT NULL,
  `correctA` int(11) DEFAULT NULL,
  `incorrectA` int(11) DEFAULT NULL,
  `answerpercent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `Question_UNIQUE` (`Question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'What is the first habit?','Be Proactive',1,0,0,'0'),(2,'What is the second habit?','Begin with the end in mind',1,0,0,'0'),(3,'What is the third habit?','Put first things first',1,0,0,'0'),(4,'What is the fourth habit?','Think Win-Win',1,0,0,'0'),(5,'What can the 7 habits help you do?','Control your life',1,0,0,'0'),(6,'What is the fifth habit?','Seek first to understand, then to be understood',1,0,0,'0'),(7,'What is the sixth habit?','Synergize',1,0,0,'0'),(8,'What is the seventh habit?','Sharpen the saw',1,0,0,'0'),(9,'What is the definition of Paradigm?','Your perceptions about the way things are.',1,0,0,'0'),(10,'You can have a Paradigm on _______','Anything',1,0,0,'0'),(11,'What is a Paradigm shift?','Where your perception about something changes.',1,0,0,'0'),(12,'You must _____ before you walk.','Crawl',1,0,0,'0'),(13,'Your personal bank account ______','Increases with good deeds',1,0,0,'0'),(14,'Your personal bank account is constantly __________','Increases and Decreases',1,0,0,'0'),(15,'Anybody can be a ________ agent','Change',1,0,0,'0'),(16,'When someone says something rude to you _________','Just hit \"pause\"',1,0,0,'0'),(17,'The Self-Awareness powertool is ','I can stand apart from myself and observe my thoughts and actions.',1,0,0,'0'),(18,'The Conscience powertool is','I can listen to my inner voice to know right from wrong.',1,0,0,'0'),(19,'The Imagination powertool is','I can envision new possibilities',1,0,0,'0'),(20,'The Willpower powertool is','I have the power to choose',1,0,0,'0'),(21,'Sharpening your mind can be as simple as ','Writing a story',1,0,0,'0'),(22,'Synergy is ________','Everywhere',1,0,0,'0'),(23,'Being Linguistic means','Learning through reading, writing, and telling stories.',1,0,0,'0'),(24,'In the Time Quadrants, being Urgent and Important is','The Procrastinator',1,0,0,'0'),(25,'In the Time Quadrants, being unimportant and Urgent is','The Prioritizer',1,0,0,'0'),(26,'In the Time Quadrants, being Urgent but not important is','The Yes-Man',1,0,0,'0'),(27,'In the Time Quadrants, being not urgent and not important is','The Slacker',1,0,0,'0'),(28,'Winning means rising each time you _______','Fall',1,0,0,'0'),(29,'First listen with your eyes, _____ , and earss','Hearts',1,1,0,'0'),(30,'Being ignorant means you are','Clueless',1,0,0,'0'),(31,'In life you have to use things or you _______','Lose them',1,0,0,'0'),(32,'The refusal skill can help when your being ','Peer pressured',1,0,0,'0'),(33,'Finding your nitch means','Working on what your not good at in order to improve.',1,0,0,'0'),(34,'Getting the grade is not as important as getting the','Education',1,0,0,'0'),(35,'The Nerd Syndrome can happen to','Anyone',1,0,0,'0'),(36,'Pressure can','Cause you do to or not do things',1,0,0,'0'),(37,'Endorphins are','Natural painkillers',1,0,0,'0'),(38,'Laugher can','Reduce stress levels',1,0,0,'0'),(39,'Making small changes to problems can help you stay','On course',1,0,0,'0'),(40,'Keeping hope alive ','Gives you hope',1,0,0,'0'),(41,'Shaking a reactive person is like shaking a ','Soda ',1,0,0,'0'),(42,'The only way to get better at something is to ______','Practice',1,0,0,'0');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticketname` varchar(45) NOT NULL,
  `ticketcreation` varchar(45) NOT NULL,
  `body` varchar(45) NOT NULL,
  `assignment` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'Open',
  `comment` varchar(45) DEFAULT NULL,
  `user` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `7_habits`.`tickets_BEFORE_INSERT` BEFORE INSERT ON `tickets` FOR EACH ROW
BEGIN
	SET NEW.ticketcreation = NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database '7_habits'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `updateleaderboards` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = '' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `updateleaderboards` ON SCHEDULE EVERY '5:0' MINUTE_SECOND STARTS '2016-04-19 08:51:42' ON COMPLETION NOT PRESERVE ENABLE DO SET @r=0 */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database '7_habits'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-27 13:34:53
