# Habits-QA
Web game developed as a class project for Government and Economics. 


# Installation

Reccommended Software:
      Nginx
      MySQL/MariaDB
      PHP-FPM
      Debian 8.0
      
Suggested Changes:
      If you choose to use Apache over nginx I would suggest ether going through the code and changing the source for individual files (there are places where I had hardcoded file locations (The big offenders are under the indexes, and under game/main/avup.htm) or using symlinks and having it "pretend" to run from nginx's default web root.

The game has no "official" installer but is rather a pull and go setup. The only thing that has to be done for you, as the host, is to setup and create the database and tables. This game will not function without the database.

I created this system in order to allow for easy manipulation to do whatever the user wanted it to do, this can easily be changed to have questions reflect topics that are of interest to the host. All the questions and answers are stored inside the database. 


Some files are going to need to be edited to allow for the system to work, besides the SQL connect.php, the 7Habits-QA/game/guest/reg.htm includes a key to allow for recaptcha to do its thing and this is not a global key, this MUST be replaced manually and is different for every site. To generate your recaptcha key go to https://www.google.com/recaptcha/

# Account Creation
Account creation is all done through the site and should not be entered into the database manually due to the way accounts are created and passwords are hashed.

The first user to be registered to the site will be granted the admin role. To add more admins there is currently not a way to do it outside of manually changing the values in the database but will be fixed in the future.

# Future Updates
While I may not seem to update this code, it does recieve some major boosts from me finding better ways to do things. The Text-based MMORPG Hacker City is based on this code and is capable of recieving secondary support from me. Most of the updated code released does not contain any of the proprietary functions or advanced features that may or may not be seen from Hacker City.

# Modifications and Re-hosting:

This version of the game is freely available to be redistributed, edited, and hosted on services not owned or affilicated by me. I permit modifications to the software without having to push back the modifications as GPL (as long as there are no monentary value created by the reuse of this base) and that a attempt was made to differentiate your copy of the source from the original (aka: Don't reuse the game's released database. The initial setup is given as a guide to help understand the engine and should not be left in use.)

Exceptions to this clause:
      I. You are using this code to republish my original 7-Habits game in a live archive
      II. You are creating a 7-Habits game and are modifying my original code to achieve a better (unique) look.
      III. You are not making a profit off of the game and its database content and are using it purely as an educational tool.

# Instances where this should not be used:

This should go without saying but I will say it anyways. If you are using this source as a way to "cheat" a grade (ex: this game was created as a high school project, therefore, it is entirely possible for someone to reuse this as their class project) do make an attempt to understand the underlying code to be able to talk about it. 
     
If you are an educator and have come across a class project that resembles this game engine AT ALL, please request that the student show game source to allow for comparison. This should not be taken as an unreasonable request as this is the Internet age and original content should be favored over direct copying.
