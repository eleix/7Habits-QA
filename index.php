<?php

session_start();
$ip = $_SERVER['REMOTE_ADDR'];
$time = $_SERVER['REQUEST_TIME'];
$timeout_duration = 1800;


if (isset($_SESSION['login_user']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
	
	$username = $_SESSION['login_user'];
	$userid = $_SESSION['userid'];
	$sql = "INSERT INTO gamelogs(userid,username,type) VALUES('".$_SESSION['userid']."', '".$_SESSION['login_user']."', 'Logged out due to inactivity')";
	$result = $conn->query($logsql);
	session_unset();
	session_destroy();
	session_start();
};

require 'sql/connect.php';

	if(!isset($_SESSION['login_user'])){

		include 'layout/guest/header.htm';
	
		$a = 'guest';
		$disallowed_paths = array('header', 'footer');
		if (!empty($_GET['a'])) {
			$tmp_action = basename($_GET['a']);
			// Checks if action is allowed and file exists, then sets the action
			if (!in_array($tmp_action, $disallowed_paths) && file_exists("game/guest/{$tmp_action}.htm")){
				$a = $tmp_action;
			};
		};
	
	include "game/guest/{$a}.htm";
	
	include "layout/guest/footer.htm";
	}
	else {
		$username = $_SESSION['login_user'];
		$userid = $_SESSION['userid'];
		
		include '/usr/share/nginx/html/layout/main/header.htm';
		
		$disallowed_paths = array('header', 'footer');
		if (!empty($_GET['m'])){
			$management = basename($_GET['m']);
			if (!in_array($management, $disallowed_paths) && file_exists("/usr/share/nginx/html/game/main/admin/{$management}.htm")){
				$m = $management;
				if ($jailtime != ''){
					if ($jailtime > $currenttime && !in_array($management, $jailoverridepaths)){
						header("location: ?a=jailed");
					};
				};
				include ("/usr/share/nginx/html/game/main/admin/{$m}.htm");
			}
		};
		if (empty($_GET['m'])){
			$a = 'home';
			$disallowed_paths = array('header', 'footer');
			if (!empty($_GET['a'])) {
				$tmp_action = basename($_GET['a']);
				// Checks if action is allowed and file exists, then sets the action
				if (!in_array($tmp_action, $disallowed_paths) && file_exists("/usr/share/nginx/html/game/main/user/{$tmp_action}.htm")){
					$a = $tmp_action;
				};
			};
			if(isset($a)){
				include "/usr/share/nginx/html/game/main/user/{$a}.htm";
			};
		}
			include "/usr/share/nginx/html/layout/main/footer.htm";
		}
		}
?>
